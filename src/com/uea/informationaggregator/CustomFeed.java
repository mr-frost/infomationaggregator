package com.uea.informationaggregator;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

/**
 * CustomFeed.java
 * 
 * A class to take a XML URL address entered by the user and display it in the
 * Custom tab
 *  
 * @author George Dee
 *
 */
public class CustomFeed extends Activity{
	
	//Variable to store URL from user - has default value
	private String url = "https://dl.dropboxusercontent.com/u/91815775/NoCustomFeed.xml";
	
	/**
	 * Method called on instantiation of object
	 * 
	 * @param savedInstanceState	previously saved state
	 */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_feed_setup);
    }

    /**
     * Method to add items to the action bar if they are present
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
    /**
     * Called when user presses Submit button
     * 
     * @param view	View from current view
     */
    public void sendMessage(View view){
    	//Intent to change from CustomFeed to MainActivity
    	Intent intent = new Intent(CustomFeed.this, MainActivity.class);
    	
    	//Extract values from text fields 
    	EditText rawTabName = (EditText) findViewById(R.id.custom_tab_name);
    	EditText rawUrl = (EditText) findViewById(R.id.custom_url);
    	   	
    	//Store url field value as String
    	url = rawUrl.getText().toString();
    	//Store name field value as String
    	String tabName = rawTabName.getText().toString();
    	
    	//If doesn't have "http://" at the start, add it
    	if(url.contains("http://") != true){
    		url = "http://" + url;
    	}
    	
    	//Validation for empty name field
    	if(tabName.trim().length() == 0){
    		Context context = getApplicationContext();
    		CharSequence text = "Please enter a name for your tab";
    		int duration = Toast.LENGTH_LONG;
    		Toast toast = Toast.makeText(context, text, duration);
    		toast.show();
    	//Validation for empty url field
    	} else if (url.trim().length() == 0) {
    		Context context = getApplicationContext();
    		CharSequence text = "Please enter a URL source for your tab";
    		int duration = Toast.LENGTH_LONG;
    		Toast toast = Toast.makeText(context, text, duration);
    		toast.show();
    	} else {
    		//Start activity if a suitable address
        	if(url.contains("www.") && url.contains(".rss") || url.contains(".xml")){
        		
            	//Store url in an intent
            	intent.putExtra("url", url);
        		//Store customTabName as global value to be used by classes
            	MainActivity.customTabName = tabName;
            	
            	//Start activity
            	startActivity(intent);
            	//Destroy previous activity
            	finish();
        	} else {
        		//Validation for url content
        		Context context = getApplicationContext();
        		CharSequence text = "Invalid URL - please enter a valid URL";
        		int duration = Toast.LENGTH_LONG;
        		Toast toast = Toast.makeText(context, text, duration);
        		toast.show();
        	}
    	
    	}
    }

}
