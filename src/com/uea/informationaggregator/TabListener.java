package com.uea.informationaggregator;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Fragment;
import android.app.FragmentTransaction;

/**
 * TabListener.java
 * 
 * A class to respond to tabs by recreating fragment if selected and 
 * removing fragment when unselected
 * 
 * @author George Dee
 *
 */
public class TabListener implements ActionBar.TabListener{
	
	private Fragment fragment;
	
	/**
	 * Constructor to construct TabListener object
	 * 
	 * @param fragment	fragment to set in TabListener object
	 */
	public TabListener(Fragment fragment){
		this.fragment = fragment;
	}
	
	/**
	 * Method to specify behaviour on selected tab
	 * 
	 * @param tab	tab selected
	 * @param ft	fragment transaction to replace fragment
	 */
	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft){
		ft.replace(R.id.activity_main, fragment);
	}
	
	/**
	 * Method to remove unselected tab
	 * 
	 * @param tab	tab unselected
	 * @param ft	fragment transaction to replace fragment
	 */
	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft){
		ft.remove(fragment);
	}
	
	/**
	 * Method to specify behaviour on reselected tab
	 */
	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft){
		
	}

}
