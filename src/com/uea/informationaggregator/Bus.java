package com.uea.informationaggregator;

/**
 * Bus.java
 * 
 * A class to construct an object of type Bus and includes methods to
 * manipulate a Bus object.
 * 
 * @author George Dee
 *
 */
public class Bus {
	
	private String route;
	private String destination;
	private String departureTime;
	private String departureStop;
	
	/**
	 * Constructor to create Bus object
	 */
	public Bus(){}
	
	/**
	 * Constructor to create Bus object
	 * 
	 * @param route			String containing value of route
	 * @param destination	String containing value of destination
	 * @param departureTime	String containing value of departure time
	 * @param departureStop	String containing value of departure stop
	 */
	public Bus(String route, String destination, String departureTime, String departureStop){
		this.route = route;
		this.destination = destination;
		this.departureTime = departureTime;
		this.departureStop = departureStop;
	}
	
	/**
	 * Get the route of the Bus object
	 * 
	 * @return	route of the Bus object
	 */
	public String getRoute(){
		return route;
	}
	
	/**
	 * Set the route of the Bus object
	 * 
	 * @param route	String containing the route to be set
	 */
	public void setRoute(String route){
		this.route = route;
	}
	
	/**
	 * Get the destination of the Bus object
	 * 
	 * @return	destination of the Bus object
	 */
	public String getDestination(){
		return destination;
	}
	
	/**
	 * Set the destination of the Bus object
	 * 
	 * @param destination	String containing the destination to be set
	 */
	public void setDestination(String destination){
		this.destination = destination;
	}
	
	/**
	 * Get the departure time of the Bus object
	 * 
	 * @return	departure time of the Bus object
	 */
	public String getDepartureTime(){
		return departureTime;
	}
	
	/**
	 * Set the departure time of the Bus object
	 * 
	 * @param departureTime	String containing the departure time to be set
	 */
	public void setDepartureTime(String departureTime){
		this.departureTime = departureTime;
	}
	
	/**
	 * Get the departure stop of the Bus object
	 * 
	 * @return	departure time of the Bus object
	 */
	public String getDepartureStop(){
		return departureStop;
	}
	
	/**
	 * Set the departure stop of the Bus object
	 * 
	 * @param departureStop	String containing the departure stop to be set
	 */
	public void setDepartureStop(String departureStop){
		this.departureStop = departureStop;
	}

}
