package com.uea.informationaggregator;

import framework.GenericXmlParser;
import android.app.Activity;
import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

/**
 * MainActivity.java
 * 
 * Method ran when application opens. Used to set up the action bar to display
 * tabs and set up tabs to show XML files.
 * 
 * @author George Dee
 *
 */
public class MainActivity extends Activity {
	
	ActionBar.Tab tab1, tab2, tab3;
	FoodList foodList;
	BusList busList;
	GenericXmlParser genericList;
	
	public static String customTabName = "Custom";
	public String url;
	public String userUrl;
	
	/**
	 * Method to initialise action bar and tabs 
	 * 
	 * @param savedInstanceState	previously saved state
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		//Initialise lists to be used
		foodList = new FoodList();
		busList = new BusList();
		genericList = new GenericXmlParser("https://dl.dropboxusercontent.com/u/91815775/NoCustomFeed.xml");
		
		//StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		//StrictMode.setThreadPolicy(policy);
		
		//Retrieve intent and extra user entered URL
		Intent in = getIntent();
		userUrl = in.getStringExtra("url");
		//If URL is not null, set as URL for custom tab
		if(userUrl != null){
			genericList.setUrl(userUrl);
		}
		
		//Retrieve the action bar
		ActionBar actionBar = getActionBar();
		
		//Set action bar settings
		actionBar.setDisplayShowHomeEnabled(true);
		actionBar.setDisplayShowTitleEnabled(false);
		
		//Create action bar tabs
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		
		//Set up custom tabs
		tab1 = actionBar.newTab().setText("Food");
		tab2 = actionBar.newTab().setText("Bus");
		tab3 = actionBar.newTab().setText(customTabName);
		
		//Set up tab listener for each tab
		tab1.setTabListener(new TabListener(foodList));
		tab2.setTabListener(new TabListener(busList));
		tab3.setTabListener(new TabListener(genericList));
		
		//Add tabs to action bar
		actionBar.addTab(tab1);
		actionBar.addTab(tab2);
		actionBar.addTab(tab3);
	}
	
	/**
	 * Method to add items to action bar if present
	 * 
	 * @param menu	used to display buttons
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	/**
	 * Method to add behaviour to buttons when pressed
	 * 
	 * @param menu	menu item to be pressed
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		ActionBar actionBar = getActionBar();
		if (id == R.id.action_settings) {
			return true;
		} else if (id == R.id.setup_feed) {
			startActivity(new Intent(MainActivity.this, CustomFeed.class));
			finish();
			return true;
		} else if (id == R.id.refresh) {
			//Determine current tab being looked and refresh content of that 
			//tab
			if(actionBar.getSelectedNavigationIndex() == tab1.getPosition()){
				foodList.onCreate(null);
			} else if(actionBar.getSelectedNavigationIndex() == tab2.getPosition()){
				busList.onCreate(null);
			} else if(actionBar.getSelectedNavigationIndex() == tab3.getPosition()){
				genericList.onCreate(null);
			} else {
				return true;
			}
		}
		return super.onOptionsItemSelected(item);
	}
}