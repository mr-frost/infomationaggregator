package com.uea.informationaggregator;

import java.util.ArrayList;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import framework.RemoteDataSource;

import android.app.ListFragment;
import android.os.Bundle;

/**
 * FoodList.java
 * 
 * A class to retrieve a food menu XML document and print out the contents of 
 * it using a custom ArrayAdapter.
 * 
 * @author George Dee
 *
 */
public class FoodList extends ListFragment {
	
	/**
	 * Constructor to create empty FoodList object
	 */
	public FoodList() {}
	
	/**
	 * Method called on instantiation which retrieves a food menu XML document
	 * and prints the contents from it using a custom ArrayAdapter
	 * 
	 * @param savedInstanceState	previously saved state
	 */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//ArrayList to hold Food objects
		ArrayList<Food> foodList = new ArrayList<Food>();
		
		//Use RemoteDataSource to get Document of given URL
		RemoteDataSource rds = RemoteDataSource.getInstance();
		Document xmlFile = rds.getResource("https://dl.dropboxusercontent.com/u/91815775/Food.xml");

		//Retrieve child node of document element
		NodeList nodeList = xmlFile.getElementsByTagName("item");
		
		//Iterate through node list, extract content from each node, set in a
		//Food object and store in Food ArrayList
		for (int i = 0; i < nodeList.getLength(); i++) {
			Element e = (Element) nodeList.item(i);
			Food food = new Food();

			food.setName(getValue(e, "name"));
			food.setPrice(getValue(e, "price"));
			food.setDescription(getValue(e, "description"));

			foodList.add(food);
		}
		
		//Add foodList to ListView
		FoodAdapter foodAdapter = new FoodAdapter(getActivity(), foodList,
				R.layout.food_list_item);
		
		//Set listAdapter of ListFragment to display list
		setListAdapter(foodAdapter);

		// // selecting single ListView item
		// ListView lv = getListView();
		//
		// lv.setOnItemClickListener(new OnItemClickListener() {
		//
		// @Override
		// public void onItemClick(AdapterView<?> parent, View view,
		// int position, long id) {
		// // getting values from selected ListItem
		// String name = ((TextView)
		// view.findViewById(R.id.name)).getText().toString();
		// String price = ((TextView)
		// view.findViewById(R.id.price)).getText().toString();
		// String description = ((TextView)
		// view.findViewById(R.id.description)).getText().toString();
		//
		// SingleFoodFragment singleFood = new SingleFoodFragment();
		// Bundle bundle = new Bundle();
		// bundle.putString("name", name);
		// bundle.putString("price", price);
		// bundle.putString("description", description);
		// singleFood.setArguments(bundle);
		//
		// }
		// });

	}
	
	/**
	 * Method to get element from a given node
	 * 
	 * @param elem	element to retrieve value from
	 * @return String containing node value
	 */
	public final String getElementValue(Node elem) {
		Node child;
		if (elem != null) {
			if (elem.hasChildNodes()) {
				for (child = elem.getFirstChild(); child != null; child = child
						.getNextSibling()) {
					if (child.getNodeType() == Node.TEXT_NODE) {
						return child.getNodeValue();
					}
				}
			}
		}
		return "";
	}
	
	/**
	 * Method to get value from an element
	 * 
	 * @param item	element to retrieve value from
	 * @param str	name of element to find
	 * @return method call to getElementValue
	 */
	public String getValue(Element item, String str) {
		NodeList n = item.getElementsByTagName(str);
		return this.getElementValue(n.item(0));
	}

}
