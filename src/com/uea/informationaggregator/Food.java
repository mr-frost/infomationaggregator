package com.uea.informationaggregator;

/**
 * Food.java
 * 
 * A class to construct an object of type Food and includes methods to
 * manipulate a Food object.
 * 
 * @author George Dee
 *
 */
public class Food {

	private String name;
	private String price;
	private String description;
	
	/**
	 * Constructor to create Food object
	 */
	public Food(){}
	
	/**
	 * Constructor to create Food object
	 * 
	 * @param name			String containing value of name
	 * @param price			String containing value of price
	 * @param description	String containing value of description
	 */
	public Food(String name, String price, String description){
		this.name = name;
		this.price = price;
		this.description = description;
	}
	
	/**
	 * Get the name of the Food object
	 * 
	 * @return	name of the Food object
	 */
	public String getName(){
		return name;
	}
	
	/**
	 * Set the name of the Food object
	 * 
	 * @param name	String containing the name to be set
	 */
	public void setName(String name){
		this.name = name;
	}
	
	/**
	 * Get the price of the Food object
	 * 
	 * @return	price of the Food object
	 */
	public String getPrice(){
		return price;
	}
	
	/**
	 * Set the price of the Food object
	 * 
	 * @param price	String containing the price to be set
	 */
	public void setPrice(String price){
		this.price = price;
	}
	
	/**
	 * Get the description of the Food object
	 * 
	 * @return	description of the Food object
	 */
	public String getDescription(){
		return description;
	}
	
	/**
	 * Set the description of the Food object
	 * 
	 * @param description	String containing the description to be set
	 */
	public void setDescription(String description){
		this.description = description;
	}
	
}
