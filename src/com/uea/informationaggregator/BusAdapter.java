package com.uea.informationaggregator;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * BusAdapter.java
 * 
 * A class to construct an Adapter which can be used to display Bus objects in
 * an ArrayList within the application.
 * 
 * @author George Dee
 *
 */
public class BusAdapter extends ArrayAdapter<Bus>{
	
	private final Context context;
	private final ArrayList<Bus> busList;
	private final int layoutResourceId;
	
	/**
	 * Constructor to create a BusAdapter object
	 * 
	 * @param context			context of Activity
	 * @param busList			ArrayList of Bus objects
	 * @param layoutResourceId	int containing id of layout resource
	 */
	public BusAdapter(Context context, ArrayList<Bus> busList, 
			int layoutResourceId){
		super(context, layoutResourceId, busList);
		this.context = context;
		this.busList = busList;
		this.layoutResourceId = layoutResourceId;
	}
	
	/**
	 * Get view to be displayed on application which shows content from busList
	 * 
	 * @param position		int containing position
	 * @param convertView	convert view
	 * @param parent		parent ViewGroup
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		View row = convertView;
		ViewHolder holder = null;
		
		if(row == null){
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = inflater.inflate(layoutResourceId, parent, false);
			
			holder = new ViewHolder();
			holder.route = (TextView)row.findViewById(R.id.route);
			holder.destination = (TextView)row.findViewById(R.id.destination);
			holder.departureTime = (TextView)row.findViewById(R.id.departuretime);
			holder.departureStop = (TextView)row.findViewById(R.id.departurestop);
			
			row.setTag(holder);
		} else {
			holder = (ViewHolder)row.getTag();
		}
		
		Bus bus = busList.get(position);
		
		holder.route.setText(bus.getRoute());
		holder.destination.setText(bus.getDestination());
		holder.departureTime.setText(bus.getDepartureTime());
		holder.departureStop.setText(bus.getDepartureStop());
		
		return row;
	}
	
	/**
	 * Class to hold TextView variables for each attribute in Bus object
	 * 
	 * @author George Dee
	 *
	 */
	static class ViewHolder
    {
        TextView route;
        TextView destination;
        TextView departureTime;
        TextView departureStop;
    }

}
