package com.uea.informationaggregator;

import android.app.Activity;
import android.app.Fragment;
import android.app.ListFragment;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class SingleFoodFragment extends ListFragment{
	
		@Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        getActivity().setContentView(R.layout.single_food_item);
	        
//	        Bundle bundle = this.getArguments();
//	        //if (bundle != null) {
//	            String name = bundle.getString("name");
//	            String price = bundle.getString("price");
//	            String description = bundle.getString("description");
//	       // }
	        
	        // getting intent data
	        //Intent in = getIntent();
	        
	        Bundle args = getArguments();
	        
	        // Get XML values from previous intent
	        String name = args.getString("name");
	        String price = args.getString("price");
	        String description = args.getString("description");
	        
	        // Displaying all values on the screen
	        TextView lblName = (TextView) getActivity().findViewById(R.id.name_label);
	        TextView lblPrice = (TextView) getActivity().findViewById(R.id.price_label);
	        TextView lblDescription = (TextView) getActivity().findViewById(R.id.description_label);
	        
	        lblName.setText(name);
	        lblPrice.setText(price);
	        lblDescription.setText(description);
	    }

}
