package com.uea.informationaggregator;

import java.util.ArrayList;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import android.app.ListActivity;
import android.app.ListFragment;
import android.os.Bundle;
import framework.RemoteDataSource;

/**
 * BusList.java
 * 
 * A class to retrieve a bus time XML document and print out the contents of it
 * using a custom ArrayAdapter.
 * 
 * @author George Dee
 *
 */
public class BusList extends ListFragment {

	/**
	 * Constructor to create empty BusList object
	 */
	public BusList() {}

	/**
	 * Method called on instantiation which retrieves a bus time XML document
	 * and prints the contents from it using a custom ArrayAdapter
	 * 
	 * @param savedInstanceState	previously saved state
	 */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		//ArrayList to hold Bus objects
		ArrayList<Bus> busList = new ArrayList<Bus>();

		//Use RemoteDataSource to get Document of given URL
		RemoteDataSource rds = RemoteDataSource.getInstance();
		Document xmlFile = rds.getResource("https://dl.dropboxusercontent.com/u/91815775/Bus.xml");

		//Retrieve child node of document element
		NodeList nodeList = xmlFile.getElementsByTagName("bustime");

		//Iterate through node list, extract content from each node, set in a
		//Bus object and store in Bus ArrayList
		for (int i = 0; i < nodeList.getLength(); i++) {
			Element e = (Element) nodeList.item(i);
			Bus bus = new Bus();

			bus.setRoute(getValue(e, "route"));
			bus.setDestination(getValue(e, "destination"));
			bus.setDepartureTime(getValue(e, "departuretime"));
			bus.setDepartureStop(getValue(e, "departurestop"));

			busList.add(bus);
		}

		//Add busList to ListView
		BusAdapter busAdapter = new BusAdapter(getActivity(), busList,
				R.layout.bus_list_item);

		//Set listAdapter of ListFragment to display list
		setListAdapter(busAdapter);

	}

	/**
	 * Method to get element from a given node
	 * 
	 * @param elem	element to retrieve value from
	 * @return String containing node value
	 */
	public final String getElementValue(Node elem) {
		Node child;
		if (elem != null) {
			if (elem.hasChildNodes()) {
				for (child = elem.getFirstChild(); child != null; child = child
						.getNextSibling()) {
					if (child.getNodeType() == Node.TEXT_NODE) {
						return child.getNodeValue();
					}
				}
			}
		}
		return "";
	}

	/**
	 * Method to get value from an element
	 * 
	 * @param item	element to retrieve value from
	 * @param str	name of element to find
	 * @return method call to getElementValue
	 */
	public String getValue(Element item, String str) {
		NodeList n = item.getElementsByTagName(str);
		return this.getElementValue(n.item(0));
	}
}
