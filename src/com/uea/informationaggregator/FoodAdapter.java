package com.uea.informationaggregator;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * FoodAdapter.java
 * 
 * A class to construct an Adapter which can be used to display Food objects in
 * an ArrayList within the application.
 * 
 * @author George Dee
 *
 */
public class FoodAdapter extends ArrayAdapter<Food> {

	private final Context context;
	private final ArrayList<Food> foodList;
	private final int layoutResourceId;
	
	/**
	 * Constructor to create a FoodAdapter object
	 * 
	 * @param context			context of Activity
	 * @param busList			ArrayList of Food objects
	 * @param layoutResourceId	int containing id of layout resource
	 */
	public FoodAdapter(Context context, ArrayList<Food> foodList,
			int layoutResourceId) {
		super(context, layoutResourceId, foodList);
		this.context = context;
		this.foodList = foodList;
		this.layoutResourceId = layoutResourceId;
	}
	
	/**
	 * Get view to be displayed on application which shows content from foodList
	 * 
	 * @param position		int containing position
	 * @param convertView	convert view
	 * @param parent		parent ViewGroup
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		ViewHolder holder = null;

		if (row == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = inflater.inflate(layoutResourceId, parent, false);

			holder = new ViewHolder();
			holder.name = (TextView) row.findViewById(R.id.name);
			holder.price = (TextView) row.findViewById(R.id.price);
			holder.description = (TextView) row.findViewById(R.id.description);

			row.setTag(holder);
		} else {
			holder = (ViewHolder) row.getTag();
		}

		Food food = foodList.get(position);

		holder.name.setText(food.getName());
		holder.price.setText(food.getPrice());
		holder.description.setText(food.getDescription());

		return row;
	}
	
	/**
	 * Class to hold TextView variables for each attribute in Food object
	 * 
	 * @author George Dee
	 *
	 */
	static class ViewHolder {
		TextView name;
		TextView price;
		TextView description;
	}

}
