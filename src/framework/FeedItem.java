package framework;

/**
 * FeedItem.java
 * 
 * Interface to provide methods for items inheriting from it. Allows for
 * further extensibility to the application.
 * 
 * @author George Dee
 *
 */
public interface FeedItem {
	
	public abstract void setContent(String content);
	public abstract void add(GenericItem item);

}
