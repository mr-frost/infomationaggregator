package test;

import com.uea.informationaggregator.Food;

import junit.framework.TestCase;

public class FoodTest extends TestCase {

	public void testGetName() {
		Food tester = new Food("Panini", "3.00", "Good food");
		assertEquals("Result", "Panini", tester.getName());
	}

	public void testSetName() {
		Food tester = new Food("Panini", "3.00", "Good food");
		tester.setName("Sausage Roll");
	}

	public void testGetPrice() {
		Food tester = new Food("Panini", "3.00", "Good food");
		assertEquals("Result", "3.00", tester.getPrice());
	}

	public void testSetPrice() {
		Food tester = new Food("Panini", "3.00", "Good food");
		tester.setPrice("1.50");
	}

	public void testGetDescription() {
		Food tester = new Food("Panini", "3.00", "Good food");
		assertEquals("Result", "Good food", tester.getDescription());
	}

	public void testSetDescription() {
		Food tester = new Food("Panini", "3.00", "Good food");
		tester.setDescription("Awesome food");
	}

}
