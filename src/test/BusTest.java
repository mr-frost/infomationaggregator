package test;

import com.uea.informationaggregator.Bus;

import junit.framework.TestCase;

public class BusTest extends TestCase {

	public void testGetRoute() {
		Bus tester = new Bus("26", "UEA", "10:00", "Rail Station");
		assertEquals("Result", "26", tester.getRoute());
	}

	public void testSetRoute() {
		Bus tester = new Bus("26", "UEA", "10:00", "Rail Station");
		tester.setRoute("25");
	}

	public void testGetDestination() {
		Bus tester = new Bus("26", "UEA", "10:00", "Rail Station");
		assertEquals("Result", "UEA", tester.getDestination());
	}

	public void testSetDestination() {
		Bus tester = new Bus("26", "UEA", "10:00", "Rail Station");
		tester.setDestination("Castle Mall");
	}

	public void testGetDepartureTime() {
		Bus tester = new Bus("26", "UEA", "10:00", "Rail Station");
		assertEquals("Result", "10:00", tester.getDepartureTime());
	}

	public void testSetDepartureTime() {
		Bus tester = new Bus("26", "UEA", "10:00", "Rail Station");
		tester.setDepartureTime("11:00");
	}

	public void testGetDepartureStop() {
		Bus tester = new Bus("26", "UEA", "10:00", "Rail Station");
		assertEquals("Result", "Rail Station", tester.getDepartureStop());
	}

	public void testSetDepartureStop() {
		Bus tester = new Bus("26", "UEA", "10:00", "Rail Station");
		tester.setDepartureStop("Main Bus Stop");
	}

}
